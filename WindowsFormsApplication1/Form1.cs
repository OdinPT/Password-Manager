﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace WindowsFormsApplication1
{

    public partial class Form1 : Form
    {

        public string senha, t;

        public int b, max, i, tamanho,a, tempx;

        List<MyStruct> emp;

        public Form1()
        {
            InitializeComponent();
            AlteraEstado(0);
            max = tamanho;

        }

        // i,senha, tamanho
        private string GeraSenha0(int tamanho)
        {

            string guid = Guid.NewGuid().ToString();
              Random pass = new Random();
              Int32 tamanhoSenha = pass.Next(tamanho);
              
              for (Int32 i = 0; i <= tamanho; i++)
              {
                   senha += guid.Substring(pass.Next(1, guid.Length), 1);
              }

            return senha;
        }

        // senha, tamanho
        private string GeraSenha(int tamanho)
        {
            const string valid = "@!#$%&{[]}'?;:~^+`*``abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder senha = new StringBuilder();
            Random rnd = new Random();
            while (0 < tamanho--)
            {
                senha.Append(valid[rnd.Next(valid.Length)]);
            }
            return senha.ToString();
            
        }

        private string GeraSenha2()
        {

            string codigoSenha = DateTime.Now.Ticks.ToString();

            senha = BitConverter.ToString(new System.Security.Cryptography.SHA512CryptoServiceProvider().ComputeHash(Encoding.Default.GetBytes(codigoSenha))).Replace("-", String.Empty);

            return senha;

        }

        // senha, tamanho
        public string GeraSenha3(int tamanho)
        {
            try
            {
                string guid = Guid.NewGuid().ToString().Replace("-", "");
                if (guid.Length > tamanho)
                {
                    senha = guid.Substring(0, tamanho);
                }
                else
                {
                    senha = guid;
                    //label8.Text = guid.Length.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return senha;

        }

        // i,senha, tamanho,max
        private void Gerar()
        {
            emp = new List<MyStruct>();
            var source = new BindingSource();

            for (i = 1; i < max; i++)
            {
                int w = i;

                if (i / 1 == 1)
                {
                    MyStruct emp1 = new MyStruct(i, GeraSenha(tamanho));
                        emp.Add(emp1);
                    i = w + 1;
                }

                if (i / 2 == 1)
                {

                    MyStruct emp3 = new MyStruct(i, GeraSenha0(tamanho));
                    emp.Add(emp3);
                    i = w + 2;
                }

                if (i / 1 == 1)
                {
                    MyStruct emp2 = new MyStruct(i, GeraSenha2());
                    emp.Add(emp2);
                    i = w + 3;
                }

                MyStruct emp4 = new MyStruct(i, GeraSenha3(tamanho));
                emp.Add(emp4);


            }

            t = GeraSenha3(tamanho);
            textBox1.Text = t;

            //a = a + 1;
            //label8.Text = a.ToString();
           // label8.Text = this.max.ToString();

            source.DataSource = emp;
            dataGridView1.DataSource = source;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Gerar();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Help frm = new Help();
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.Show();
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        // i
        private void AlteraEstado(int i)
        {
            switch (i) {
                case 0:
                    label3.Visible = false;
                    label7.Visible = false;
                    button1.Visible = false;
                    label5.Visible = false;
                    label8.Visible = false;
                    dataGridView1.Visible = false;
                    label8.Text = " ";
                    break;
                case 1:
                    button1.Visible = true;
                    label3.Visible = true;
                    label5.Visible = true;
                    dataGridView1.Visible = true;
                    label8.Visible = true;
                    label7.Visible = true;
                    break;
                case 2:
                    dataGridView1.Visible = false;
                    button1.Visible = false;

                    label3.Visible = false;
                    label5.Visible = false;
                    label8.Visible = false;
                    label7.Visible = false;
                    break;
                case 3:
                    dataGridView1.Visible = false;
                    button1.Visible = true;

                    label3.Visible = true;
                    label5.Visible = true;
                    label8.Visible = true;
                    label7.Visible = true;
                    break;
            }
    }           
        //tamanho,
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                AlteraEstado(3);

                this.tamanho = 16;

                label3.Text = tamanho.ToString();
                checkBox1.Checked.ToString();

            }
            else if (!checkBox1.Checked)
            {
                AlteraEstado(3);
                //button1.Visible = true;
                checkBox1.CheckState.ToString();
                this.tamanho = 24;  
                label3.Text = tamanho.ToString();

            }
             if (checkBox1.Checked && checkBox2.Checked)
            {
                AlteraEstado(3);

                this.tamanho = 40;
                label3.Text = tamanho.ToString();
                checkBox1.Checked.ToString();
                checkBox2.Checked.ToString();
            }
              else if (!checkBox1.Checked && !checkBox2.Checked)
                {
                    AlteraEstado(0);
               
                    textBox1.Text = " ";
                    this.tamanho = 0;

                    label3.Text = tamanho.ToString();
                    checkBox1.Checked.ToString();
                    checkBox2.Checked.ToString();
                }
        }

        //tamanho
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
             {
                AlteraEstado(3);

                this.tamanho = 24;
                label3.Text = tamanho.ToString();
                checkBox2.Checked.ToString();
             }
                else if (!checkBox2.Checked)
                {
                    AlteraEstado(3);
                    checkBox1.CheckState.ToString();
                    this.tamanho = 16;
                    label3.Text = tamanho.ToString();

                }
             if (checkBox2.Checked && checkBox1.Checked)
            {
                AlteraEstado(1);
               
                this.tamanho = 40;
                
                label3.Text = tamanho.ToString();
                checkBox2.Checked.ToString();
                checkBox1.Checked.ToString();
            }
            else if (!checkBox1.Checked && !checkBox2.Checked)
            {
                AlteraEstado(2);

                textBox1.Text = " ";

                this.tamanho = 0;
                Console.WriteLine(tamanho);
                label3.Text = tamanho.ToString();

                checkBox1.Checked.ToString();
                checkBox2.Checked.ToString();
            }

        }

        //max
        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            int tempx;

            if (checkBox4.Checked)
            {
                this.max = 5;
                tempx = 4;

                AlteraEstado(1);

                label8.Text = tempx.ToString();
                
            }
            else if (!checkBox4.Checked)
            {
                this.max=6;
                tempx =5;

                label8.Text = tempx.ToString();
            }

            if (checkBox4.Checked && checkBox3.Checked)
            {
                int t;

                AlteraEstado(1);
                max = 11;
                t = max - 1;

                label8.Text = t.ToString();
                checkBox4.Checked.ToString();
                checkBox3.Checked.ToString();
            }
             if (!checkBox3.Checked && !checkBox4.Checked)
            {
                AlteraEstado(3);

                textBox1.Text = " ";

                this.max = 0;
                Console.WriteLine(max);

                label8.Text = max.ToString();

                checkBox3.Checked.ToString();
                checkBox4.Checked.ToString();
            }
        }

        //max
        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            int tempx;
            
            if (checkBox3.Checked)
            {
                AlteraEstado(1);

                max = 7;
                tempx = max - 1;
                
                label8.Text = tempx.ToString();
            }
            else if (!checkBox3.Checked)
            {
                this.max = 5;
                tempx = max - 1;

                label8.Text = this.tempx.ToString();
            }


            if (checkBox3.Checked && checkBox4.Checked)
            {
                int tx;
                AlteraEstado(1);

                this.max = 11;
                tx = max - 1;

                //Console.WriteLine(max);

                label8.Text = t.ToString();
                checkBox3.Checked.ToString();
                checkBox4.Checked.ToString();
            }

            if (!checkBox3.Checked && !checkBox4.Checked)
            {
                AlteraEstado(3);

                textBox1.Text = " ";

                this.max = 0;
                label8.Text = max.ToString();
                checkBox3.Checked.ToString();
                checkBox4.Checked.ToString();
            }
        }
    }
}