﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class MyStruct
    {
        public int Id { get; set; }
        public string Senha { get; set; }


        public MyStruct(int id, string senha)
        {
            Id = id;
            Senha = senha;
        }

        public override string ToString()
        {
            string temp = " ";
            temp += Id;
            temp += Senha;

            return temp;
        }
    }
}
